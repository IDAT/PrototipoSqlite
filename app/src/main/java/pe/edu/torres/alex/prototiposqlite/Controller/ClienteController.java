package pe.edu.torres.alex.prototiposqlite.Controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import pe.edu.torres.alex.prototiposqlite.Bean.ClienteBean;
import pe.edu.torres.alex.prototiposqlite.DAO.DataBaseHelper;
import pe.edu.torres.alex.prototiposqlite.Model.ClienteModel;

public class ClienteController extends DataBaseHelper{

    public ClienteController(Context context) {
        super(context);
    }

    public JSONObject selClienteSync(String strURL){
        JSONObject jsonObjectResult = new JSONObject();
        JSONObject jsonObjectResponse =null;
        try{
            Log.i("IDAT",strURL);
            URL url = new URL(strURL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setUseCaches(false);
            //httpURLConnection.setConnectTimeout(10000); // 10 Seg
            //httpURLConnection.setReadTimeout(15000); // 15 Seg
            httpURLConnection.setRequestMethod("GET");
            InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8");
            BufferedReader in = new BufferedReader(inputStreamReader);
            String strLine;
            StringBuffer stringBuffer = new StringBuffer();

            while ((strLine = in.readLine()) != null) {
                stringBuffer.append(strLine);
            }
            //Log.i("IDAT",stringBuffer.toString());
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                jsonObjectResponse = new JSONObject(stringBuffer.toString());
            }

            if(jsonObjectResponse.getBoolean("status")) {
                StringBuilder stbSQL = new StringBuilder()
                        .append("INSERT OR REPLACE INTO CLDMCLIE ")
                        .append("(CLMCCODCIA, CLMCCODSUC, CLMCCODCLI, CLMCNRORUC, CLMCNOMCLI, CLMCCODVND, CLMCNROTEL, CLMCNROFAX,")
                        .append("CLMCEMAIL,CLMCESTCLI, CLMCFECAUV,CLMCODSEC,CLMCODSSEC,CLMCCODCLA,DIRLATITUD,DIRLONGITUD,")
                        .append("CLMCCODFORP,CLMCCONENT,CLMCCONVTA,CLMCCODMON,CLMCCODTERR,CLMCDESFORP,CLMDESCONENT,CLMDESCONVTA) ")
                        .append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?)");
                SQLiteDatabase myDataBase = getWritableDatabase();
                myDataBase.execSQL("PRAGMA synchronous=OFF");
                myDataBase.execSQL("PRAGMA count_changes=OFF");
                myDataBase.setLockingEnabled(false);
                myDataBase.beginTransaction();
                SQLiteStatement stmt = myDataBase.compileStatement(stbSQL.toString());

                for(int i=0;i < jsonObjectResponse.getJSONArray("clientes").length();i++){
                    Log.i("JSON ARRAY ITEM", jsonObjectResponse.getJSONArray("clientes").get(i).toString());
                    //TODO - CREAMOS UN JSONOBJECT PARA CONTENER EL ITEM DEL JSONARRAY QUE ES UN JSONOBJECT
                    JSONObject Item = jsonObjectResponse.getJSONArray("clientes").getJSONObject(i);
                    stmt.bindString(1, "001");
                    stmt.bindString(2, "001");
                    stmt.bindString(3, Item.getString("CLMCCODCLI"));
                    stmt.bindString(4, Item.getString("CLMCNEWRUC"));
                    stmt.bindString(5, Item.getString("CLMCNOMCLI"));
                    stmt.bindString(6, String.valueOf("PPT01"));
                    stmt.bindString(7, Item.getString("CLMCNROTEL"));
                    stmt.bindString(8, Item.getString("CLMCNROFAX"));
                    stmt.bindString(9, Item.getString("CLMCEMAIL"));
                    stmt.bindString(10, Item.getString("CLMCESTCLI"));
                    stmt.bindString(11, Item.getString("CLMCFECAUV"));
                    stmt.bindString(12, Item.getString("CLMCODSEC"));
                    stmt.bindString(13, Item.getString("CLMCODSSEC"));
                    stmt.bindString(14, Item.getString("CLMCCODCLA"));
                    stmt.bindString(15, Item.getString("DIRLATITUD"));
                    stmt.bindString(16, Item.getString("DIRLONGITUD"));
                    stmt.bindString(17, Item.getString("CLMCCODFORP"));
                    stmt.bindString(18, Item.getString("CLMCCONENT"));
                    stmt.bindString(19, Item.getString("CLMCCONVTA"));
                    stmt.bindString(20, Item.getString("CLMCCODMON"));
                    stmt.bindString(21, Item.getString("CLMCCODTERR"));
                    stmt.bindString(22, Item.getString("CLMCDESFORP"));
                    stmt.bindString(23, Item.getString("CLMDESCONENT"));
                    stmt.bindString(24, Item.getString("CLMDESCONVTA"));
                    stmt.execute();
                    stmt.clearBindings();
                }

                myDataBase.setTransactionSuccessful();
                myDataBase.endTransaction();
                myDataBase.setLockingEnabled(true);
                myDataBase.execSQL("PRAGMA synchronous=NORMAL");
            }

            //TODO - CREAMOS UN JSON PARA SOLO DEVOLVER EL STATUS Y MESSAGE
            jsonObjectResult.accumulate("status", jsonObjectResponse.getBoolean("status"));
            jsonObjectResult.accumulate("message", jsonObjectResponse.getString("message"));

        }catch(Exception e){
            e.printStackTrace();
            try {
                jsonObjectResult.accumulate("status", false);
                jsonObjectResult.accumulate("message", e.getMessage());
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        return jsonObjectResult;
    }

    public ArrayList<ClienteBean> selCliente(){
        ArrayList<ClienteBean> lst = new ArrayList<>();
        Cursor cursor = null;
        try {
            SQLiteDatabase myDataBase = getWritableDatabase();
            String[] campos = new String[] {"CLMCCODCLI", "CLMCNRORUC","CLMCNOMCLI","CLMCCODVND","CLMCNROTEL","CLMCNROFAX","CLMCEMAIL","CLMCESTCLI",
                    "CLMCFECAUV","CLMCODSEC","CLMCODSSEC","CLMCCODCLA","DIRLATITUD","DIRLONGITUD",
                    "CLMCCODFORP", "CLMCCONENT","CLMCCONVTA","CLMCCODMON","CLMCCODTERR","CLMCDESFORP","CLMDESCONENT","CLMDESCONVTA"};

            cursor = myDataBase.query("CLDMCLIE", campos, "CLMCCODVND= ?", new String[]{"PPT01"}, null, null, "CLMCNOMCLI");
            lst = new ArrayList<ClienteBean>();
            lst.clear();
            if (cursor.moveToFirst()) {
                do {
                    ClienteBean item = new ClienteBean();
                    item.setCLMCCODCLI(cursor.isNull(cursor.getColumnIndex("CLMCCODCLI")) ? "" : cursor.getString(cursor.getColumnIndex("CLMCCODCLI")));
                    item.setCLMCNOMCLI(cursor.isNull(cursor.getColumnIndex("CLMCNOMCLI")) ? "" : cursor.getString(cursor.getColumnIndex("CLMCNOMCLI")));
                    lst.add(item);
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return lst;
    }
}
