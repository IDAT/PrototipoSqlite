package pe.edu.torres.alex.prototiposqlite.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import pe.edu.torres.alex.prototiposqlite.Bean.ClienteBean;
import pe.edu.torres.alex.prototiposqlite.R;



public class Cliente_Recycler_Adapter extends RecyclerView.Adapter<Cliente_Recycler_Adapter.View_Holder> {

    List list = Collections.emptyList();
    Context context;

    public Cliente_Recycler_Adapter(List list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cliente, parent, false);
        View_Holder holder = new View_Holder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(View_Holder holder, int position) {
        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        ClienteBean item =(ClienteBean) list.get(position);
        holder.bindCliente(item);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public static class View_Holder extends RecyclerView.ViewHolder {
        private TextView txv1,txv2;
        View_Holder(View itemView) {
            super(itemView);
            txv1 =(TextView) itemView.findViewById(R.id.txvNombre);
            txv2 =(TextView) itemView.findViewById(R.id.txvDescri);
        }
        public void bindCliente(ClienteBean c) {
            txv1.setText(c.getCLMCCODCLI());
            txv2.setText(c.getCLMCNOMCLI());
        }
    }
}