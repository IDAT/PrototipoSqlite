package pe.edu.torres.alex.prototiposqlite.DAO;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION =    5;
    private static final String DATABASE_NAME = "dblogistica";
    private static final String TABLE_CLIENTS = "CLDMCLIE";
    private static final String TABLE_PRODUCTS = "CRMPRODUCTOS";

    private static final StringBuilder stb_CREATE_CLIENTS = new StringBuilder()
            .append("CREATE TABLE " + TABLE_CLIENTS + " ")
            .append("(CLMCCODCIA TEXT, CLMCCODSUC TEXT, CLMCCODCLI CHAR(20) PRIMARY KEY, CLMCNRORUC CHAR(11), ")
            .append("CLMCNOMCLI TEXT, CLMCCODVND TEXT, CLMCNROTEL TEXT, CLMCNROFAX TEXT, ")

            .append("CLMCEMAIL TEXT,  CLMCESTCLI TEXT, CLMCFECAUV TEXT, CLMCODSEC TEXT, ")
            .append("CLMCODSSEC TEXT, CLMCCODCLA TEXT, DIRLATITUD TEXT, DIRLONGITUD TEXT, ")

            .append("CLMCCODFORP TEXT, CLMCCONENT TEXT, CLMCCONVTA TEXT, CLMCCODMON TEXT, ")
            .append("CLMCCODTERR TEXT, CLMCDESFORP TEXT, CLMDESCONENT TEXT, CLMDESCONVTA TEXT) ");

    private static final StringBuilder stb_CREATE_PRODUCTS = new StringBuilder()
            .append("CREATE TABLE " + TABLE_PRODUCTS + " ")
            .append("(MADCODCIA TEXT, MADCODSUC TEXT, MADCODART TEXT, MADDESART TEXT, ")
            .append("MSDSTKDIS TEXT, MSDSTKBLO TEXT, MSDSTKCUA TEXT, MSDSTKCMP TEXT, ")
            .append("MADUNIVEN TEXT, MADCODVEN TEXT, MADUNIMED TEXT, MADMON TEXT) ");


    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.i("LOG","CONSTRUCTOR");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            //TODO - CREACION DE TABLE CLIENTES
            sqLiteDatabase.execSQL(stb_CREATE_CLIENTS.toString());

            //TODO - CREACION DE TABLE PRODUCTOS
            sqLiteDatabase.execSQL(stb_CREATE_CLIENTS.toString());
        }catch (SQLiteException e){
            e.printStackTrace();
            Log.i("LOG", e.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.i("LOG","UPGRADE");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENTS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        onCreate(sqLiteDatabase);
    }

}