package pe.edu.torres.alex.prototiposqlite;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pe.edu.torres.alex.prototiposqlite.Adapter.Cliente_Recycler_Adapter;
import pe.edu.torres.alex.prototiposqlite.Bean.ClienteBean;
import pe.edu.torres.alex.prototiposqlite.Controller.ClienteController;
import pe.edu.torres.alex.prototiposqlite.Util.UriConstantesUtil;

public class MainActivity extends AppCompatActivity {
    //TODO - ACTIVITY VARIABLES
    FloatingActionButton fab;
    FloatingActionButton fab1;
    FloatingActionButton fab2;
    FloatingActionButton fab3;

    //Save the FAB's active status
    //false -> fab = close
    //true -> fab = open
    private boolean FAB_Status = false;

    //Animations
    Animation show_fab_1;
    Animation hide_fab_1;
    Animation show_fab_2;
    Animation hide_fab_2;
    Animation show_fab_3;
    Animation hide_fab_3;

    View rootLayout;
    RecyclerView rcv;
    ClienteController clienteController = new ClienteController(this);

    ArrayList<ClienteBean> aryClientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TODO - ENLAZAR OBJECT XML A OBJECT JAVA
        rootLayout = findViewById(R.id.MyCoordinatorLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab1 = (FloatingActionButton) findViewById(R.id.fab_1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab_2);
        fab3 = (FloatingActionButton) findViewById(R.id.fab_3);

        //Animations
        show_fab_1 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show);
        hide_fab_1 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide);
        show_fab_2 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_show);
        hide_fab_2 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_hide);
        show_fab_3 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_show);
        hide_fab_3 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_hide);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (FAB_Status == false) {
                    //Display FAB menu
                    expandFAB();
                    FAB_Status = true;
                } else {
                    //Close FAB menu
                    hideFAB();
                    FAB_Status = false;
                }
            }
        });
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplication(), "Boton Nuevo", Toast.LENGTH_SHORT).show();
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplication(), "Consultar Clientes desde SQLite", Toast.LENGTH_SHORT).show();
                aryClientes = clienteController.selCliente();
                Cliente_Recycler_Adapter adapter = new Cliente_Recycler_Adapter(aryClientes, getApplication());
                rcv.setAdapter(adapter);

            }
        });

        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplication(), "Sincronizar Clientes desde Servicio Rest", Toast.LENGTH_SHORT).show();
                //TODO - LLAMAR A LA TAREA ASYNCTASK
                new HttpRequestTask().execute(UriConstantesUtil.RESTFUL_URL + UriConstantesUtil.CLIENTE_URI + "/ppt01");
            }
        });

        rcv = (RecyclerView) findViewById(R.id.rcvClientes);
        rcv.setLayoutManager(new LinearLayoutManager(this));

        rcv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (FAB_Status) {
                    hideFAB();
                    FAB_Status = false;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class HttpRequestTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setTitle("Aviso");
            dialog.setMessage("Cargando Clientes...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... argumento) {
            return clienteController.selClienteSync( argumento[0]);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                //TODO - CERRAR PROGRESSDIALOG SI ESTA VISIBLE
                if(dialog.isShowing())dialog.dismiss();

                //TODO - MOSTRAR KEY MESSAGE
                Snackbar.make(rootLayout, jsonObject.getString("message") ,Snackbar.LENGTH_LONG).show();
                if(jsonObject.getBoolean("status") && jsonObject.has("data")) {

                    //TODO - OBTENER JSONARRAY DEL JSONOBJECT
                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    aryClientes = new ArrayList<>();

                    for (int x = 0; x < jsonArray.length(); x++) {
                        //TODO - OBTENER JSONOBJECT DEL JSONARRAY
                        JSONObject i = jsonArray.getJSONObject(x);
                        ClienteBean c = new ClienteBean();
                        //c.setUsu_codigo(i.getString("xxx"));
                        aryClientes.add(c);
                    }

                    //TODO - CREAR ARRAYADAPTER
                    //clienteAdapter = new ClienteAdapter(MainActivity.this, R.layout.layout_cliente, aryClientes);
                    //clienteAdapter.notifyDataSetChanged();

                    //TODO - CARGAR ARRAYADAPTER A LISTVIEW
                    //lv.setAdapter(clienteAdapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void expandFAB() {

        //Floating Action Button 1
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
        layoutParams.rightMargin += (int) (fab1.getWidth() * 1.7);
        layoutParams.bottomMargin += (int) (fab1.getHeight() * 0.25);
        fab1.setLayoutParams(layoutParams);
        fab1.startAnimation(show_fab_1);
        fab1.setClickable(true);

        //Floating Action Button 2
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
        layoutParams2.rightMargin += (int) (fab2.getWidth() * 1.5);
        layoutParams2.bottomMargin += (int) (fab2.getHeight() * 1.5);
        fab2.setLayoutParams(layoutParams2);
        fab2.startAnimation(show_fab_2);
        fab2.setClickable(true);

        //Floating Action Button 3
        FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
        layoutParams3.rightMargin += (int) (fab3.getWidth() * 0.25);
        layoutParams3.bottomMargin += (int) (fab3.getHeight() * 1.7);
        fab3.setLayoutParams(layoutParams3);
        fab3.startAnimation(show_fab_3);
        fab3.setClickable(true);
    }

    private void hideFAB() {

        //Floating Action Button 1
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
        layoutParams.rightMargin -= (int) (fab1.getWidth() * 1.7);
        layoutParams.bottomMargin -= (int) (fab1.getHeight() * 0.25);
        fab1.setLayoutParams(layoutParams);
        fab1.startAnimation(hide_fab_1);
        fab1.setClickable(false);

        //Floating Action Button 2
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
        layoutParams2.rightMargin -= (int) (fab2.getWidth() * 1.5);
        layoutParams2.bottomMargin -= (int) (fab2.getHeight() * 1.5);
        fab2.setLayoutParams(layoutParams2);
        fab2.startAnimation(hide_fab_2);
        fab2.setClickable(false);

        //Floating Action Button 3
        FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
        layoutParams3.rightMargin -= (int) (fab3.getWidth() * 0.25);
        layoutParams3.bottomMargin -= (int) (fab3.getHeight() * 1.7);
        fab3.setLayoutParams(layoutParams3);
        fab3.startAnimation(hide_fab_3);
        fab3.setClickable(false);
    }

}
