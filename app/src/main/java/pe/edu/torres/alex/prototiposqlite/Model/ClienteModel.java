package pe.edu.torres.alex.prototiposqlite.Model;

public class ClienteModel {
    private String CLMCCODCIA;
    private String CLMCCODSUC;
    private String CLMCCODCLI;//
    private String CLMCNRORUC;//
    private String CLMCNOMCLI;//
    private String CLMCCODVND; //
    private String CLMCODSEC;//
    private String CLMCODSSEC;//
    private String CLMCCODCLA;//
    private String DIRLATITUD;//
    private String DIRLONGITUD;//
    private String CLMCNROTEL;//
    private String CLMCNROFAX;//
    private String CLMCEMAIL;//
    private String CLMCESTCLI;//
    private String CLMCFECAUV;//
    private String CLMCCODFORP;//-
    private String CLMCCONENT;//-
    private String CLMCCONVTA;//-
    private String CLMCCODMON;//
    private String CLMCCODTERR;//
    private String CLMCDESFORP;
    private String CLMDESCONENT;
    private String CLMDESCONVTA;

    public ClienteModel() {
    }

    public String getCLMCCODCIA() {
        return CLMCCODCIA;
    }

    public void setCLMCCODCIA(String CLMCCODCIA) {
        this.CLMCCODCIA = CLMCCODCIA;
    }

    public String getCLMCCODSUC() {
        return CLMCCODSUC;
    }

    public void setCLMCCODSUC(String CLMCCODSUC) {
        this.CLMCCODSUC = CLMCCODSUC;
    }

    public String getCLMCCODCLI() {
        return CLMCCODCLI;
    }

    public void setCLMCCODCLI(String CLMCCODCLI) {
        this.CLMCCODCLI = CLMCCODCLI;
    }

    public String getCLMCNRORUC() {
        return CLMCNRORUC;
    }

    public void setCLMCNRORUC(String CLMCNRORUC) {
        this.CLMCNRORUC = CLMCNRORUC;
    }

    public String getCLMCNOMCLI() {
        return CLMCNOMCLI;
    }

    public void setCLMCNOMCLI(String CLMCNOMCLI) {
        this.CLMCNOMCLI = CLMCNOMCLI;
    }

    public String getCLMCCODVND() {
        return CLMCCODVND;
    }

    public void setCLMCCODVND(String CLMCCODVND) {
        this.CLMCCODVND = CLMCCODVND;
    }

    public String getCLMCODSEC() {
        return CLMCODSEC;
    }

    public void setCLMCODSEC(String CLMCODSEC) {
        this.CLMCODSEC = CLMCODSEC;
    }

    public String getCLMCODSSEC() {
        return CLMCODSSEC;
    }

    public void setCLMCODSSEC(String CLMCODSSEC) {
        this.CLMCODSSEC = CLMCODSSEC;
    }

    public String getCLMCCODCLA() {
        return CLMCCODCLA;
    }

    public void setCLMCCODCLA(String CLMCCODCLA) {
        this.CLMCCODCLA = CLMCCODCLA;
    }

    public String getDIRLATITUD() {
        return DIRLATITUD;
    }

    public void setDIRLATITUD(String DIRLATITUD) {
        this.DIRLATITUD = DIRLATITUD;
    }

    public String getDIRLONGITUD() {
        return DIRLONGITUD;
    }

    public void setDIRLONGITUD(String DIRLONGITUD) {
        this.DIRLONGITUD = DIRLONGITUD;
    }

    public String getCLMCNROTEL() {
        return CLMCNROTEL;
    }

    public void setCLMCNROTEL(String CLMCNROTEL) {
        this.CLMCNROTEL = CLMCNROTEL;
    }

    public String getCLMCNROFAX() {
        return CLMCNROFAX;
    }

    public void setCLMCNROFAX(String CLMCNROFAX) {
        this.CLMCNROFAX = CLMCNROFAX;
    }

    public String getCLMCEMAIL() {
        return CLMCEMAIL;
    }

    public void setCLMCEMAIL(String CLMCEMAIL) {
        this.CLMCEMAIL = CLMCEMAIL;
    }

    public String getCLMCESTCLI() {
        return CLMCESTCLI;
    }

    public void setCLMCESTCLI(String CLMCESTCLI) {
        this.CLMCESTCLI = CLMCESTCLI;
    }

    public String getCLMCFECAUV() {
        return CLMCFECAUV;
    }

    public void setCLMCFECAUV(String CLMCFECAUV) {
        this.CLMCFECAUV = CLMCFECAUV;
    }

    public String getCLMCCODFORP() {
        return CLMCCODFORP;
    }

    public void setCLMCCODFORP(String CLMCCODFORP) {
        this.CLMCCODFORP = CLMCCODFORP;
    }

    public String getCLMCCONENT() {
        return CLMCCONENT;
    }

    public void setCLMCCONENT(String CLMCCONENT) {
        this.CLMCCONENT = CLMCCONENT;
    }

    public String getCLMCCONVTA() {
        return CLMCCONVTA;
    }

    public void setCLMCCONVTA(String CLMCCONVTA) {
        this.CLMCCONVTA = CLMCCONVTA;
    }

    public String getCLMCCODMON() {
        return CLMCCODMON;
    }

    public void setCLMCCODMON(String CLMCCODMON) {
        this.CLMCCODMON = CLMCCODMON;
    }

    public String getCLMCCODTERR() {
        return CLMCCODTERR;
    }

    public void setCLMCCODTERR(String CLMCCODTERR) {
        this.CLMCCODTERR = CLMCCODTERR;
    }

    public String getCLMCDESFORP() {
        return CLMCDESFORP;
    }

    public void setCLMCDESFORP(String CLMCDESFORP) {
        this.CLMCDESFORP = CLMCDESFORP;
    }

    public String getCLMDESCONENT() {
        return CLMDESCONENT;
    }

    public void setCLMDESCONENT(String CLMDESCONENT) {
        this.CLMDESCONENT = CLMDESCONENT;
    }

    public String getCLMDESCONVTA() {
        return CLMDESCONVTA;
    }

    public void setCLMDESCONVTA(String CLMDESCONVTA) {
        this.CLMDESCONVTA = CLMDESCONVTA;
    }
}
