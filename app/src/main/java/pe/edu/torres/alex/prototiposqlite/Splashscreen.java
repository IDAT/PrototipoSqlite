package pe.edu.torres.alex.prototiposqlite;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Splashscreen extends Activity {

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    Thread splashTread;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        TextView txv =(TextView)findViewById(R.id.splash_text);
        //TODO - CARGAR FONT TTF
        //Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/archistico_bold.ttf");
        Typeface tf =  ResourcesCompat.getFont(this, R.font.antonio_bold);

        //TODO - APLICAR FUENTE TTF
        txv.setTypeface(tf);

        //TODO - INICIAR ANIMACION
        StartAnimations();
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l=(LinearLayout) findViewById(R.id.splash_home);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView igv = (ImageView) findViewById(R.id.splash_logo);
        TextView txv =(TextView)findViewById(R.id.splash_text);
        igv.clearAnimation();
        igv.startAnimation(anim);

        txv.clearAnimation();
        txv.startAnimation(anim);

        //TODO - CREAR UN HILO THREAD PARA CARGAR MAINACTIVITY
        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    //TODO - SPLASH SCREEN TIEMPO DE PAUSA
                    while (waited < 6000) {
                        sleep(100);
                        waited += 100;
                    }
                    Intent intent = new Intent(Splashscreen.this,MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);

                    Splashscreen.this.finish();

                } catch (InterruptedException e) {
                } finally {
                    Splashscreen.this.finish();
                }
            }
        };
        //TODO - INICIAR EL HILO
        splashTread.start();
    }
}